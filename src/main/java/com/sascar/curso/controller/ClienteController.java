package com.sascar.curso.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sascar.curso.entity.Cliente;

@RestController
@RequestMapping(value = "/cliente")
public class ClienteController {
	
	@RequestMapping(method = RequestMethod.GET)
	private ResponseEntity<List<Cliente>> findAll() {
		List<Cliente> clientes = new ArrayList<>();
		
		Cliente cl1 = new Cliente();
		cl1.setId(1);
		cl1.setNome("Heitor");
		cl1.setSobrenome("Maciel");
		cl1.setIdade(27);
		
		Cliente cl2 = new Cliente();
		cl2.setId(2);
		cl2.setNome("Renan");
		cl2.setSobrenome("Manoel");
		cl2.setIdade(31);
		
		clientes.add(cl1);
		clientes.add(cl2);
		
		return ResponseEntity.ok().body(clientes);
	}
}
