package com.sascar.curso.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sascar.curso.entity.Categoria;
import com.sascar.curso.service.CategoryService;

@RestController
@RequestMapping(value = "/categoria")
public class CategoryController {
	@Autowired
	public CategoryService categoryService;

	@RequestMapping(method = RequestMethod.GET)
	private ResponseEntity<List<Categoria>> getAll() {
		List<Categoria> category = categoryService.findAll();
		return ResponseEntity.ok().body(category);
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	private ResponseEntity<Categoria> findById(@PathVariable Long id) {
		Optional<Categoria> categoria = categoryService.findById(id);
		
		return ResponseEntity.ok().body(categoria.get());
	}
	
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	private ResponseEntity<Void> insert(@RequestBody Categoria categoria) {
		categoryService.insert(categoria);
		
		URI	uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(categoria.getId()).toUri();
				
		return ResponseEntity.created(uri).build();
	}
	
}
