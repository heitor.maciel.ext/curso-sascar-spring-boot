package com.sascar.curso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursoSascarApplication {

	public static void main(String[] args) {
		SpringApplication.run(CursoSascarApplication.class, args);
	}

}
