package com.sascar.curso.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sascar.curso.entity.Categoria;
import com.sascar.curso.repository.CategoryRepository;

@Service
public class CategoryService {
	@Autowired
	public CategoryRepository categoryRepository;
	
	public List<Categoria> findAll() {
		return categoryRepository.findAll();
	}
	
	public Optional<Categoria> findById(Long id) {
		Optional<Categoria> cat = categoryRepository.findById(id);
		return cat;
	}
	
	public void insert(Categoria category) {
		categoryRepository.save(category);
	}
}
