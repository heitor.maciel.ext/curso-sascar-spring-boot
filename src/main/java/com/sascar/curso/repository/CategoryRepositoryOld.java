package com.sascar.curso.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sascar.curso.entity.Categoria;

@Repository
public class CategoryRepositoryOld {
	private static final String stringConnection = "jdbc:postgresql://localhost:5432/spring-boot";
	private static final String userConnection = "postgres";
	private static final String passwordConnection = "maciel1992";
	
	public List<Categoria> findAll() {

		List<Categoria> categorias = new ArrayList<>();

		String SQL_SELECT = "SELECT * FROM PUBLIC.CATEGORIAS;";

		try (Connection conn = DriverManager.getConnection(stringConnection, userConnection, passwordConnection); 
				PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				long id = resultSet.getLong("id");
				String nome = resultSet.getString("nome");

				Categoria obj = new Categoria();
				obj.setId(id);
				obj.setNome(nome);

				categorias.add(obj);

			}
			
			return categorias;

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Categoria findById(Long id) {
		Categoria category = null;
		String SQL_SELECT = "SELECT * FROM PUBLIC.CATEGORIAS WHERE id = " + id + ";";

		try (Connection conn = DriverManager.getConnection(stringConnection, userConnection, passwordConnection); 
				PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				long idResult = resultSet.getLong("id");
				String nome = resultSet.getString("nome");

				category = new Categoria();
				category.setId(idResult);
				category.setNome(nome);
			}
			
			return category;

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		return null;
	}
	
	public void insert(Categoria category) {
		String SQL_SELECT = String.format("INSERT INTO PUBLIC.CATEGORIAS SELECT %s, '%s';", category.getId(), category.getNome());
		
		try (Connection conn = DriverManager.getConnection(stringConnection, userConnection, passwordConnection); 
				PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

			preparedStatement.executeQuery();
		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
}
