package com.sascar.curso.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sascar.curso.entity.Categoria;

public interface CategoryRepository extends JpaRepository<Categoria, Long> {

}
